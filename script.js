/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
для створення:
document.createElement('p')
додавання:
appendChild()
prependChild()
insertAdjacentElement('afterbegin', span), 
або ще з (beforebegin, afterbegin, beforeend, afterend)

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
1. отримати елемент - = const navigation=document.querySelector('.navigation')
2. видаляємо елемент -  navigation.remove()

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
знайти цей елемент, наприклад це button
button.insertAdjacentElement('beforebegin', span)
або
button.insertAdjacentElement('afterend', span)
 */
// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
const link = document.createElement('a')
link.textContent = "Learn More";
link.setAttribute('src', '#')
const footer = document.querySelector('footer')
footer.append(link)


//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const select = document.createElement('select')
select.setAttribute('id', 'rating');
let fragment = document.createDocumentFragment()
for (let i = 0; i < 4; i++){
    const option = document.createElement('option');
    option.textContent = `${i+1}`
    option.setAttribute('value', `${i+1}`)
    fragment.append(option)
}
select.append(fragment)
const main = document.querySelector('main')
main.prepend(select)
//дякую! дійсно корочше набагато))

// const option1 = document.createElement('option');
// option1.textContent = '4'
// option1.setAttribute('value', '4')
// const option2 = document.createElement('option');
// option2.textContent = '3'
// option2.setAttribute('value', '3')

// const option3 = document.createElement('option');
// option3.textContent = '2'
// option3.setAttribute('value', '2')

// const option4 = document.createElement('option');
// option4.textContent = '1'
// option4.setAttribute('value', '1')
// select.append(option1)
// select.append(option2)
// select.append(option3)
// select.append(option4)














//CLASS WORK


/**
 * Завдання 1.
 *
 * Написати скрипт, який створить елемент button із текстом «Увійти».
 *
 * При натисканні на кнопці виводити alert з повідомленням: «Ласкаво просимо!».
 */
/**
 * Завдання 2.
 *
 * Поліпшити скрипт із попереднього завдання.
 *
 * При наведенні на кнопку вказівником миші виводити alert з повідомленням:
 * «При натисканні кнопки ви увійдете в систему.».
 *
 * Повідомлення має виводитися один раз.
 *
 * Умови:
 * - Розв'язати завдання грамотно.
 */

// const button = document.createElement('button')
// button.classList.add('btn')
// button.textContent = 'Увійти';

// button.addEventListener('click', () =>{
//     alert('Ласкаво просимо!')
// })

// button.addEventListener('mouseover', () => {
//     alert('При натисканні кнопки ви увійдете в систему.')
// }, {once: true})


// document.body.append(button)

/**
 * Завдання 3.
 *
 * Створити елемент h1 з текстом «Ласкаво просимо!».
 *
 * Під елементом h1 додати елемент button з текстом «Розфарбувати».
 *
 * При натисканні кнопки змінювати колір кожної літери елемента h1 на випадковий.
 */
// **
//  * Завдання 4.
//  *
//  * Поліпшити скрипт із попереднього завдання.
//  *
//  * При кожному русі курсору по сторінці
//  * Змінювати колір кожної літери елемента h1 на випадковий.
//  */

// const PHRASE = 'Ласкаво просимо!';
// function getRandomColor() {
//     const r = Math.floor(Math.random() * 255);
//     const g = Math.floor(Math.random() * 255);
//     const b = Math.floor(Math.random() * 255);
//     return `rgb(${r}, ${g}, ${b})`;
// }

// const h1 = document.createElement('h1');
// h1.textContent = PHRASE;
// const btn = document.createElement('button');
// btn.textContent = 'Розфарбувати';
// btn.addEventListener('mouse', changeColor)
// document.body.addEventListener('mousemove', changeColor)
// document.body.append(h1)
// document.body.append(btn)

// function changeColor(){
//     console.log(h1.textContent)
//     const text = h1.textContent
//     h1.textContent = ''
//     for (let ch of text){
//         const span = document.createElement('span')
//         span.textContent = ch
//         span.style.color = getRandomColor()
//         h1.append(span)
//     }
    
// }

